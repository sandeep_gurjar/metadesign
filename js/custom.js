const month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd ',
});

$(document).on('click', '.delete-row', function(){
    $(this).closest('tr').remove();
});
$(document).on('click', '.mark-as-done', function(){
    $(this).closest('tr').addClass('done');
});

$(document).on('click', '#save', function(){
    let dob = $('#datepicker').val();
    let fullname = $('#fullname').val();
    let nickname = $('#nickname').val();
    if(dob=='' || fullname=='' || nickname==''){
        $('#errorMsg').text('All fields are required.')
        return;
    }
    const d = new Date(dob);
    let displayDate = month[d.getMonth()]+' '+d.getDate()+', '+d.getYear();
    let newrow = '<tr><td><a class="ul-widget4__title d-block text-capitalize" href="">'+fullname+'</a><span>'+displayDate+'</span> <strong class="pipe">|</strong><span class="text-capitalize">'+nickname+'</span></td> <td><span></span></td><td><label class="checkbox mt-3 checkbox-outline-info"><input type="checkbox" checked=""><span class="checkmark"></span></label></td> <td> <button class="btn mt-2 _r_btn border-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="_dot _inline-dot bg-primary"></span><span class="_dot _inline-dot bg-primary"></span><span class="_dot _inline-dot bg-primary"></span></button><div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;"><a class="dropdown-item ul-widget__link--font mark-as-done" href="javascript:void(0);"><i class="i-Bar-Chart-4"> </i> Mark as Done</a><a class="dropdown-item ul-widget__link--font delete-row" href="javascript:void(0);"><i class="i-Duplicate-Layer"></i> Delete</a><div class="dropdown-divider"></div> </div></td></tr>';
    $("#mainTable tr:first").after(newrow);
    $('#my_form')[0].reset();
    $("#exampleModal").modal('hide');
    $('#errorMsg').text('');
});